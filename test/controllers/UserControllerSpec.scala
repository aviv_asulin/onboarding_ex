package controllers

import java.util.Calendar
import com.typesafe.config.ConfigFactory
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.Configuration
import play.api.test._
import play.api.test.Helpers._
import org.scalatest._
import Matchers._
import org.joda.time.format.DateTimeFormat
import play.api.libs.json.JsArray
import play.api.libs.ws.WSClient
import play.api.mvc.Result
import scala.concurrent.Future

class UserControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  def validateUsersResponse(response: Future[Result]): Unit = {
    status(response) mustBe OK
    contentType(response) mustBe Some("application/json")

    val jsonResponse = contentAsJson(response).as[JsArray]
    jsonResponse.value.length should be > 0
    jsonResponse.value should not be empty

    val username = (jsonResponse.value.last \ "username").get.toString
    val id = (jsonResponse.value.last \ "id").get.toString
    username.length should be > 0
    id.length should be > 0
  }

  def validateCommitsResponse(response: Future[Result]): Unit = {
    status(response) mustBe OK
    contentType(response) mustBe Some("application/json")

    val jsonResponse = contentAsJson(response).as[JsArray]
    jsonResponse.value.length should be > 0
    jsonResponse.value should not be empty

    val hash = (jsonResponse.value.last \ "hash").get.as[String]
    hash should not be empty

    val repoName = (jsonResponse.value.last \ "repository_name").get.as[String]
    repoName should not be empty

    val message = (jsonResponse.value.last \ "message").get.as[String]
    message should not be empty


    val date = (jsonResponse.value.last \ "date").get.as[String]
    val yesterday = Calendar.getInstance()
    yesterday.add(Calendar.HOUR, -24)
    val commitDateObj = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ").parseDateTime(date)
    commitDateObj.getMillis.toInt must be >= yesterday.get(Calendar.MILLISECOND)
  }


  "UserController GET" should {
    val username = "teamcity"

    "return Users from a new instance of controller" in {
      val controller = new UserController(
        app.injector.instanceOf[WSClient],
        stubControllerComponents(),
        Configuration(ConfigFactory.load()))
      val usersResponse = controller.getUsers().apply(FakeRequest(GET, "/api/users"))
      println("fake request responmse:")
      println(usersResponse)
      validateUsersResponse(usersResponse)
    }

    "return Users from from the application" in {
      val controller = inject[UserController]
      val usersResponse = controller.getUsers().apply(FakeRequest(GET, "/api/users"))

      validateUsersResponse(usersResponse)
    }

    "return Users from the router" in {
      val request = FakeRequest(GET, "/api/users")
      val usersResponse = route(app, request).get
      validateUsersResponse(usersResponse)
    }

    "return Commits from a new instance of controller" in {
      val controller = new UserController(
        app.injector.instanceOf[WSClient],
        stubControllerComponents(),
        Configuration(ConfigFactory.load()))
      val commitsResponse = controller.getUserCommits(username).apply(FakeRequest(GET, "/api/users/" + username + "/commits"))
      validateCommitsResponse(commitsResponse)
    }

    "return Commits from from the application" in {
      val controller = inject[UserController]
      val commitsResponse = controller.getUserCommits(username).apply(FakeRequest(GET, "/api/users/" + username + "/commits"))

      validateCommitsResponse(commitsResponse)
    }

    "return Commits from the router" in {
      val request = FakeRequest(GET, "/api/users/" + username + "/commits")
      val commitsResponse = route(app, request).get

      validateCommitsResponse(commitsResponse)
    }
  }
}
