package controllers

import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test._
import play.api.test.Helpers._

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 *
 * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
 */
class HealthControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  "HealthController GET" should {

    "return OK from a new instance of controller" in {
      val controller = new HealthController(stubControllerComponents())
      val healthResponse = controller.healthCheck().apply(FakeRequest(GET, "/api/health"))
      status(healthResponse) mustBe OK
      contentType(healthResponse) mustBe Some("text/plain")
    }

    "return OK from from the application" in {
      val controller = inject[HealthController]
      val healthResponse = controller.healthCheck().apply(FakeRequest(GET, "/api/health"))

      status(healthResponse) mustBe OK
      contentType(healthResponse) mustBe Some("text/plain")
    }

    "return OK from the router" in {
      val request = FakeRequest(GET, "/api/health")
      val healthResponse = route(app, request).get

      status(healthResponse) mustBe OK
      contentType(healthResponse) mustBe Some("text/plain")
    }
  }
}
