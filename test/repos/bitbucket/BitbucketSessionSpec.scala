package repos.bitbucket

import java.text.SimpleDateFormat
import java.util.Calendar
import org.scalatestplus.play.guice._
import play.api.test._
import org.scalatest._
import Matchers._
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play._
import org.mockito.Mockito._
import ExecutionContext.Implicits.global

class BitbucketSessionSpec extends PlaySpec with GuiceOneServerPerSuite with Injecting with MockitoSugar {

  val testUser = new BitbucketUser("12345", "test_user")
  val otherUser = new BitbucketUser("56789", "other_user")

  val cal = Calendar.getInstance
  val now = cal.getTime

  val yearsFormat = new SimpleDateFormat("yyyy")
  val monthsFormat = new SimpleDateFormat("MM")
  val daysFormat = new SimpleDateFormat("dd")
  val hoursFormat = new SimpleDateFormat("HH")
  val minutesFormat = new SimpleDateFormat("mm")
  val secondsFormat = new SimpleDateFormat("ss")
  val timezoneFormat = new SimpleDateFormat("ss")
  val currentYear = yearsFormat.format(now)
  val currentMonth = monthsFormat.format(now)
  val currentDay = daysFormat.format(now)
  val currentHour = hoursFormat.format(now)
  val currentMinutes = minutesFormat.format(now)
  val currentSeconds = secondsFormat.format(now)

  val nowString = s"${currentYear}-${currentMonth}-${currentDay}T${currentHour}:${currentMinutes}:${currentSeconds}"

  cal.add(Calendar.HOUR, -25)
  val yesterday = cal.getTime
  val yesterdayYear = yearsFormat.format(yesterday)
  val yesterdayMonth = monthsFormat.format(yesterday)
  val yesterdayDay = daysFormat.format(yesterday)
  val yesterdayHour = hoursFormat.format(yesterday)
  val yesterdayMinutes = minutesFormat.format(yesterday)
  val yesterdaySeconds = secondsFormat.format(yesterday)

  val yesterdayString = s"${yesterdayYear}-${yesterdayMonth}-${yesterdayDay}T${yesterdayHour}:${yesterdayMinutes}:${yesterdaySeconds}"

  val currentTZ = "+03:00"

  "Bitbucket Session" should {

    "return an empty users collection" in {
      // init
      val service = mock[BitbucketService]
      val session = new BitbucketSession(service)

      // setup
      val NumOfPages = 1
      when(service.getNumUsersPages).thenReturn(Future(NumOfPages))
      when(service.getUsersByPage(1)).thenReturn(Future(Seq()))

      // run test
      val users = Await.result(session.getUsers, 5 seconds)

      // verify
      users must have size 0
    }

    "return 1 user" in {
      // init
      val service = mock[BitbucketService]
      val session = new BitbucketSession(service)

      // setup
      val NumOfPages = 1
      when(service.getNumUsersPages).thenReturn(Future(NumOfPages))
      when(service.getUsersByPage(1)).thenReturn(Future(Seq(testUser)))

      // run test
      val users = Await.result(session.getUsers, 5 seconds)

      // verify
      users must have size 1
      users(0) == testUser shouldBe true
    }

    "return multiple users" in {
      // init
      val service = mock[BitbucketService]
      val session = new BitbucketSession(service)

      // setup
      val testUsers = (1 to 10).map(i => BitbucketUser("12345_" + i, "test_user_" + 1))
      val NumOfPages = 1
      when(service.getNumUsersPages).thenReturn(Future(NumOfPages))
      when(service.getUsersByPage(1)).thenReturn(Future(testUsers))

      // run test
      val users = Await.result(session.getUsers, 5 seconds)

      // verify
      users must have size 10
      (0 to 9).foreach(i => users(i) == testUsers(i) shouldBe true)
    }

    "return users from 2 pages" in {
      // init
      val service = mock[BitbucketService]
      val session = new BitbucketSession(service)

      // setup
      val testUsers1 = (1 to 10).map(i => BitbucketUser("12345_" + i, "test_user_" + i))
      val testUsers2 = (11 to 20).map(i => BitbucketUser("12345_" + i, "test_user_" + i))
      val pages = 2
      when(service.getNumUsersPages).thenReturn(Future(pages))
      when(service.getUsersByPage(1)).thenReturn(Future(testUsers1))
      when(service.getUsersByPage(2)).thenReturn(Future(testUsers2))

      // run test
      val users = Await.result(session.getUsers, 5 seconds)

      // verify
      val testUsersCombined = testUsers1 ++ testUsers2
      users must have size 20
      (0 to 19).foreach(i => users(i) == testUsersCombined(i) shouldBe true)
    }

    "return users from 3 pages" in {
      // init
      val service = mock[BitbucketService]
      val session = new BitbucketSession(service)

      // setup
      val testUsers1 = (1 to 10).map(i => BitbucketUser("12345_" + i, "test_user_" + i))
      val testUsers2 = (11 to 20).map(i => BitbucketUser("12345_" + i, "test_user_" + i))
      val testUsers3 = (21 to 30).map(i => BitbucketUser("12345_" + i, "test_user_" + i))
      val pages = 3
      when(service.getNumUsersPages).thenReturn(Future(pages))
      when(service.getUsersByPage(1)).thenReturn(Future(testUsers1))
      when(service.getUsersByPage(2)).thenReturn(Future(testUsers2))
      when(service.getUsersByPage(3)).thenReturn(Future(testUsers3))

      // run test
      val users = Await.result(session.getUsers, 5 seconds)

      // verify
      val testUsersCombined = testUsers1 ++ testUsers2 ++ testUsers3
      users must have size 30
      (0 to 29).foreach(i => users(i) == testUsersCombined(i) shouldBe true)
    }

    "return users from pages with different amount in each page" in {
      // init
      val service = mock[BitbucketService]
      val session = new BitbucketSession(service)

      // setup
      val testUsers1 = (1 to 10).map(i => BitbucketUser("12345_" + i, "test_user_" + i))
      val testUsers2 = (11 to 15).map(i => BitbucketUser("12345_" + i, "test_user_" + i))
      val pages = 2
      when(service.getNumUsersPages).thenReturn(Future(pages))
      when(service.getUsersByPage(1)).thenReturn(Future(testUsers1))
      when(service.getUsersByPage(2)).thenReturn(Future(testUsers2))

      // run test
      val users = Await.result(session.getUsers, 5 seconds)

      // verify
      val testUsersCombined = testUsers1 ++ testUsers2
      users must have size 15
      (0 to 14).foreach(i => users(i) == testUsersCombined(i) shouldBe true)
    }

    "return users from 3 pages with no users on last page" in {
      // init
      val service = mock[BitbucketService]
      val session = new BitbucketSession(service)

      // setup
      val testUsers1 = (1 to 10).map(i => BitbucketUser("12345_" + i, "test_user_" + i))
      val testUsers2 = (11 to 20).map(i => BitbucketUser("12345_" + i, "test_user_" + i))
      val pages = 3
      when(service.getNumUsersPages).thenReturn(Future(pages))
      when(service.getUsersByPage(1)).thenReturn(Future(testUsers1))
      when(service.getUsersByPage(2)).thenReturn(Future(testUsers2))
      when(service.getUsersByPage(3)).thenReturn(Future(Seq()))

      // run test
      val users = Await.result(session.getUsers, 5 seconds)

      // verify
      val testUsersCombined = testUsers1 ++ testUsers2
      users must have size 20
      (0 to 19).foreach(i => users(i) == testUsersCombined(i) shouldBe true)
    }

    "return no commits" in {
      // init
      val service = mock[BitbucketService]
      val session = new BitbucketSession(service)

      // setup
      val testUser = new BitbucketUser("12345", "test_user")
      val yesterdayString = s"${yesterdayYear}-${yesterdayMonth}-${yesterdayDay}T${yesterdayHour}:${yesterdayMinutes}:${yesterdaySeconds}"
      val testRepo = new BitbucketRepository(
        "12345678",
        "test_repo",
        yesterdayString
      )

      when(service.getRepositories).thenReturn(Future(Seq(testRepo)))
      when(service.getCommitsByRepository(testRepo, 1)).thenReturn(Future(Seq()))

      // run test
      val commits = Await.result(session.getUserCommits(testUser.id), 5 seconds)

      // verify
      commits must have size 0
    }

    "return no commits from the last 24H" in {
      // init
      val service = mock[BitbucketService]
      val session = new BitbucketSession(service)

      // setup
      val testUser = new BitbucketUser("12345", "test_user")
      val yesterdayString = s"${yesterdayYear}-${yesterdayMonth}-${yesterdayDay}T${yesterdayHour}:${yesterdayMinutes}:${yesterdaySeconds}"
      val testRepo = new BitbucketRepository(
        "12345678",
        "test_repo",
        yesterdayString
      )
      val testCommit = new BitbucketCommit(
        "cnu2iy4785y2hc",
        yesterdayString + currentTZ,
        testRepo.name,
        "Test message",
        testUser
      )

      when(service.getRepositories).thenReturn(Future(Seq(testRepo)))
      when(service.getCommitsByRepository(testRepo, 1)).thenReturn(Future(Seq(testCommit)))

      // run test
      val commits = Await.result(session.getUserCommits(testUser.id), 5 seconds)

      // verify
      commits must have size 0
    }

    "return 1 commit from the last 24H" in {
      // init
      val service = mock[BitbucketService]
      val session = new BitbucketSession(service)

      // setup
      val testRepo = new BitbucketRepository(
        "12345678",
        "test_repo",
        nowString
      )
      val testCommit = new BitbucketCommit(
        "cnu2iy4785y2hc",
        nowString + currentTZ,
        testRepo.name,
        "Test message",
        testUser
      )

      when(service.getRepositories).thenReturn(Future(Seq(testRepo)))
      when(service.getCommitsByRepository(testRepo, 1)).thenReturn(Future(Seq(testCommit)))
      when(service.getCommitsByRepository(testRepo, 2)).thenReturn(Future(Seq()))

      // run test
      val commits = Await.result(session.getUserCommits(testUser.id), 5 seconds)

      // verify
      commits must have size 1
      commits(0) == testCommit shouldBe true
    }

    "return commits from the last 24H" in {
      // init
      val service = mock[BitbucketService]
      val session = new BitbucketSession(service)

      // setup
      val testUser = new BitbucketUser("12345", "test_user")
      val testRepo = new BitbucketRepository(
        "12345678",
        "test_repo",
        nowString
      )
      val testCommits = (1 to 10).map(i => new BitbucketCommit(
        "cnu2iy4785y2hc_" + i,
        nowString + currentTZ,
        testRepo.name,
        "Test message_" + i,
        testUser
      ))

      when(service.getRepositories).thenReturn(Future(Seq(testRepo)))
      when(service.getCommitsByRepository(testRepo, 1)).thenReturn(Future(testCommits))
      when(service.getCommitsByRepository(testRepo, 2)).thenReturn(Future(Seq()))

      // run test
      val commits = Await.result(session.getUserCommits(testUser.id), 5 seconds)

      // verify
      commits must have size 10
      (0 to 9).foreach(i => commits(i) == testCommits(i) shouldBe true)
    }

    "return commits from the last 24H from multiple repos" in {
      // init
      val service = mock[BitbucketService]
      val session = new BitbucketSession(service)

      // setup
      val testUser = new BitbucketUser("12345", "test_user")
      val testRepos = (1 to 10).map(i => new BitbucketRepository(
        "12345678_" + i,
        "test_repo_" + i,
        nowString
      ))
      val reposCommits = testRepos.map(repo => {
        (1 to 10).map(commitIndex => new BitbucketCommit(
          "cnu2iy4785y2hc_" + commitIndex,
          nowString + currentTZ,
          repo.name,
          "Test message_" + commitIndex,
          testUser
        ))
      })

      when(service.getRepositories).thenReturn(Future(testRepos))
      (0 to 9).map(i => {
        when(service.getCommitsByRepository(testRepos(i), 1)).thenReturn(Future(reposCommits(i)))
        when(service.getCommitsByRepository(testRepos(i), 2)).thenReturn(Future(Seq()))
      })

      // run test
      val commits = Await.result(session.getUserCommits(testUser.id), 5 seconds)

      // verify
      commits must have size 100
      val combinedCommits = reposCommits.flatten
      (0 to 99).foreach(i => commits(i) == combinedCommits(i) shouldBe true)
    }

    "return commits from the last 24H from multiple repos and filter other users" in {
      // init
      val service = mock[BitbucketService]
      val session = new BitbucketSession(service)

      // setup
      val testRepos = (1 to 10).map(i => new BitbucketRepository(
        "12345678_" + i,
        "test_repo_" + i,
        nowString
      ))
      val reposCommits = testRepos.map(repo => {
        (1 to 10).map(commitIndex => new BitbucketCommit(
          "cnu2iy4785y2hc_" + commitIndex,
          nowString + currentTZ,
          repo.name,
          "Test message_" + commitIndex,
          if (commitIndex % 2 == 0) testUser else otherUser
        ))
      })

      when(service.getRepositories).thenReturn(Future(testRepos))
      (0 to 9).map(i => {
        when(service.getCommitsByRepository(testRepos(i), 1)).thenReturn(Future(reposCommits(i)))
        when(service.getCommitsByRepository(testRepos(i), 2)).thenReturn(Future(Seq()))
      })

      // run test
      val commits = Await.result(session.getUserCommits(testUser.id), 5 seconds)

      // verify
      commits must have size 50
      val combinedCommits = reposCommits.flatten.filter(c => c.user == testUser)
      (0 to 49).foreach(i => commits(i) == combinedCommits(i) shouldBe true)
    }

    "return commits from the last 24H from multiple repos and filter other users && past 24H valid commits only" in {
      // init
      val service = mock[BitbucketService]
      val session = new BitbucketSession(service)

      // setup
      val testRepos = (1 to 10).map(i => new BitbucketRepository(
        "12345678_" + i,
        "test_repo_" + i,
        nowString
      ))
      val nowCommitStr = nowString + currentTZ
      val yesterdayCommitStr = yesterdayString + currentTZ
      val reposCommits = testRepos.map(repo => {
        (1 to 10).map(commitIndex => new BitbucketCommit(
          "cnu2iy4785y2hc_" + commitIndex,
          if (commitIndex % 10 == 0) nowCommitStr else yesterdayCommitStr,
          repo.name,
          "Test message_" + commitIndex,
          if (commitIndex % 2 == 0) testUser else otherUser
        ))
      })

      when(service.getRepositories).thenReturn(Future(testRepos))
      (0 to 9).map(i => {
        when(service.getCommitsByRepository(testRepos(i), 1)).thenReturn(Future(reposCommits(i)))
        when(service.getCommitsByRepository(testRepos(i), 2)).thenReturn(Future(Seq()))
      })

      // run test
      val commits = Await.result(session.getUserCommits(testUser.id), 5 seconds)

      // verify
      val combinedCommits = reposCommits.flatten.filter(c => c.user == testUser && c.date == nowCommitStr)
      println("combinedCommits length: " + combinedCommits.length)
      commits must have size 10
      (0 to 9).foreach(i => commits(i) == combinedCommits(i) shouldBe true)
    }
  }
}
