package utilities

import java.util.{Calendar, Date}
import java.net.URLEncoder
import org.joda.time.format.DateTimeFormat
import java.nio.charset.StandardCharsets

object DateParser {
  def yesterday: Date = {
    val cal = Calendar.getInstance
    cal.add(Calendar.HOUR, -24)
    cal.getTime
  }

  def weeksAgo(num: Int = 1): Date = {
    val cal = Calendar.getInstance
    cal.add(Calendar.DATE, -7 * num)
    cal.getTime
  }

  def isAfterYesterday(dateStr: String, pattern: String = "yyyy-MM-dd'T'HH:mm:ssZ"): Boolean = {
    val dtf = DateTimeFormat.forPattern(pattern)
    val dateTime = dtf.parseDateTime(dateStr)
    val date = dateTime.toDate()
    date.after(this.yesterday)
  }
}

object UrlParser {
  def encodeUrl(url: String): String =
    URLEncoder.encode(url, StandardCharsets.UTF_8.toString)
}
