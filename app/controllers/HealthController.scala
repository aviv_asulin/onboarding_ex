package controllers

import javax.inject._
import play.api.mvc._

@Singleton
class HealthController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def healthCheck() = Action { implicit request: Request[AnyContent] => {
    Ok("Status: OK")
  } }
}
