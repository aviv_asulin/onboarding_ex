package controllers

import javax.inject._
import play.api.Configuration
import play.api.libs.json.Json
import play.api.libs.ws._
import play.api.mvc._

import scala.concurrent._
import ExecutionContext.Implicits.global
import repos.Session
import repos.bitbucket.BitbucketService

@Singleton
class UserController @Inject()(ws: WSClient, cc: ControllerComponents, config: Configuration) extends AbstractController(cc) {

  def getUsers = Action.async {
    Session(BitbucketService(ws,config)).getUsers.map(members => {
      Ok(Json.toJson(members))
        .withHeaders("Access-Control-Allow-Origin" -> "*", "Access-Control-Allow-Header" -> "*")
    })
  }

  def getUserCommits(userId: String) = Action.async {
    Session(BitbucketService(ws,config)).getUserCommits(userId).map(commits => {
      Ok(Json.toJson(commits))
        .withHeaders("Access-Control-Allow-Origin" -> "*", "Access-Control-Allow-Header" -> "*")
    })
  }
}
