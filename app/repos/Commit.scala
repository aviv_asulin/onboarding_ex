package repos

import play.api.libs.json.{Json, Writes}

trait Commit {
  val hash: String
  val date: String
  val repository_name: String
  val message: String
  val user: User
}

object Commit {
  implicit val CommitWrites = new Writes[Commit] {
    def writes(commit: Commit) = Json.obj(
      "hash" -> commit.hash,
      "date" -> commit.date,
      "repository_name" -> commit.repository_name,
      "message" -> commit.message
    )
  }
}