package repos

import repos.bitbucket.BitbucketRepository

trait Repository {
  val name: String
  val id: String
  val updatedOn: String
}

object Repository {
  def apply(id: String,name: String, updatedOn: String): Repository = {
    new BitbucketRepository(id,name,updatedOn)
  }
}
