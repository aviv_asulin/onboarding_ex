package repos.bitbucket

import play.api.libs.json.{JsValue, Json, Writes}
import repos.Commit

class BitbucketCommit(val hash: String, val date: String, val repository_name: String, val message: String, val user: BitbucketUser) extends Commit {

  override def equals(that: Any): Boolean =
    that match {
      case that: BitbucketCommit => {
        that.isInstanceOf[BitbucketCommit] &&
          this.hash == that.hash &&
          this.date == that.date &&
          this.repository_name == that.repository_name &&
          this.message == that.message &&
          this.user == that.user
      }
      case _ => false
    }
}


object BitbucketCommit {
  implicit val bitbucketCommitWrites = new Writes[BitbucketCommit] {
    def writes(bitbucketCommit: BitbucketCommit) = Json.obj(
      "hash" -> bitbucketCommit.hash,
      "date" -> bitbucketCommit.date,
      "repository_name" -> bitbucketCommit.repository_name,
      "message" -> bitbucketCommit.message
    )
  }

  def fromJson(commitJson: JsValue): BitbucketCommit = {
    val hash = commitJson("hash").as[String]
    val date = commitJson("date").as[String]
    val repoObject = commitJson("repository").as[JsValue]
    val repoName = repoObject("name").as[String]
    val msg = commitJson("message").as[String]
    val authorJson = commitJson("author").as[JsValue]
    var user = new BitbucketUser("", "")
    try {
      val userJson = authorJson("user").as[JsValue]
      val id = userJson("account_id").as[String]
      val username = userJson("nickname").as[String]
      user = new BitbucketUser(id, username)
    }
    catch {
      // in case of failure to parse user from json object:
      // set user to teamcity (bitbucket api does not have a real user for teamcity commits)
      case _: Throwable => user = new BitbucketUser("", "teamcity")
    }
    new BitbucketCommit(hash, date, repoName, msg, user)
  }

}