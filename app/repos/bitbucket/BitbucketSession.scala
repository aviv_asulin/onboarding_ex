package repos.bitbucket

import play.api.Logging
import repos.{Session, User}
import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.language.postfixOps
import utilities.DateParser
import scala.concurrent.duration._

class BitbucketSession(bitbucketService: BitbucketService) extends Session with Logging {

  def getUsers(): Future[Seq[User]] = {
    bitbucketService.getNumUsersPages.flatMap(pages => {
      Future.sequence(
        (1 to pages).map(page => bitbucketService.getUsersByPage(page))
      ).map(_.flatten)
    })
  }

  def getUserCommits(userId: String): Future[Seq[BitbucketCommit]] = {
    bitbucketService.getRepositories().flatMap(repos => {
      val futures = repos
        .filter(repo => DateParser.isAfterYesterday(repo.updatedOn, "yyyy-MM-dd'T'HH:mm:ss"))
        .map(repo =>
          get24HCommitsByRepository(repo)
            .map(commits =>
              commits.filter(c =>
                c.user.id == userId || c.user.username == userId
              )))
      Future.sequence(futures).map(_.flatten)
    })
  }

  private def get24HCommitsByRepository(repository: BitbucketRepository, pageNum: Int = 1): Future[Seq[BitbucketCommit]] = {
    val commits = Await.result(bitbucketService.getCommitsByRepository(repository, pageNum), 3 minute)
    var past24HCommits = commits.filter(c => DateParser.isAfterYesterday(c.date))
    past24HCommits.foreach(c =>
      println("commit " + c.date + " is after yesterday? "  + DateParser.isAfterYesterday(c.date)))

    def shouldRequestNextPage = commits.length > 0 && past24HCommits.length == commits.length

    if (shouldRequestNextPage) {
      past24HCommits ++= Await.result(get24HCommitsByRepository(repository, pageNum + 1), 3 minute)
    }

    Future[Seq[BitbucketCommit]] {
      past24HCommits
    }
  }
}
