package repos.bitbucket

import play.api.libs.json.Writes
import play.api.libs.json.{JsValue, Json}
import repos.{Repository, Session}

class BitbucketRepository(val id: String, val name: String, val updatedOn: String) extends Repository {
  override def toString = s"Bitbucket repo: ${name} (id: ${id}). (last update: " + updatedOn + ")"
}


object BitbucketRepository {
  implicit val bitbucketRepositoryWrites = new Writes[BitbucketRepository] {
    def writes(bitbucketRepository: BitbucketRepository) = Json.obj(
      "name" -> bitbucketRepository.name,
      "id" -> bitbucketRepository.id,
      "updatedOn" -> bitbucketRepository.updatedOn
    )
  }

  def fromJson(repoJson: JsValue): BitbucketRepository = {
    val id = repoJson("uuid").as[String]
    val name = repoJson("name").as[String]
    val updatedOn = """\.""".r.split(repoJson("updated_on").as[String])(0)
    new BitbucketRepository(id, name, updatedOn)
  }
}
