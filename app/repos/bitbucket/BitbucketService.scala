package repos.bitbucket

import play.api.{Configuration, Logging}
import play.api.libs.json.JsArray
import play.api.libs.ws.{WSAuthScheme, WSClient}
import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.language.postfixOps
import scala.concurrent.Future

class BitbucketService(ws: WSClient, config: Configuration) extends Logging {

  val username: String = config.get[String]("BITBUCKET_USER")
  val pwd: String = config.get[String]("BITBUCKET_APP_PWD")
  val innovid = config.get[String]("BITBUCKET_TEAM")
  val baseURL = "https://bitbucket.org/api/2.0/"

  def getRepositories(): Future[Seq[BitbucketRepository]] = {
    getNumReposPages().flatMap(pages =>
      Future.sequence((1 to pages).map(page => getReposByPage(page)))
        .map(_.flatten))
  }

  def getReposByPage(pageNum: Int): Future[Seq[BitbucketRepository]] = {
    val url = baseURL + "repositories/" + innovid + "?page=" + pageNum
    get(url).map(response =>
      response.json("values").as[JsArray].value.map(repoJson =>
        BitbucketRepository.fromJson(repoJson))
        .toSeq
    )
  }

  def getUsersByPage(pageNum: Int): Future[Seq[BitbucketUser]] = {
    val url = baseURL + "teams/" + innovid + "/members?page=" + pageNum
    get(url).map(response =>
      response.json("values").as[JsArray].value
        .filter(userJson => userJson("type").as[String] == "user")
        .map(userJson => BitbucketUser.fromJson(userJson)).toSeq
    )
  }

  def getCommitsByRepository(repository: BitbucketRepository, pageNum: Int): Future[Seq[BitbucketCommit]] = {
    val url = baseURL + "repositories/" + innovid + "/" + repository.id + "/commits?page=" + pageNum
    get(url).map(response =>
      response.json("values").as[JsArray].value
        .map(commitJson => BitbucketCommit.fromJson(commitJson)).toSeq
    )
  }

  def getNumUsersPages(): Future[Int] = {
    getNumPages("teams/" + innovid + "/members")
  }

  def getNumReposPages(): Future[Int] = {
    getNumPages("repositories/" + innovid)
  }

  private def getNumPages(restPath: String): Future[Int] = {
    val url = baseURL + restPath
    get(url).map(response => (math.ceil(response.json("size").as[Int].toDouble / response.json("pagelen").as[Int])).toInt)
  }

  private def get(url: String) = {
    logger debug ("sending GET req: " + url)
    println("sending GET req: " + url)
    ws.url(url).withAuth(username, pwd, WSAuthScheme.BASIC).get
  }
}

object BitbucketService {
  def apply(ws: WSClient, config: Configuration): BitbucketService = {
    new BitbucketService(ws, config)
  }
}
