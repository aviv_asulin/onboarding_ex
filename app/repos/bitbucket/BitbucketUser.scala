package repos.bitbucket

import play.api.libs.json.{Writes}
import play.api.libs.json.{JsValue, Json}

import repos.User

case class BitbucketUser(val id: String, val username: String) extends User {

  override def toString = s"Bitbucket user: ${username} (id: ${id})."

  override def equals(that: Any): Boolean =
    that match {
      case that: BitbucketUser => {
        that.isInstanceOf[BitbucketUser] &&
          this.username == that.username &&
          this.id == that.id
      }
      case _ => false
    }
}


object BitbucketUser {
  implicit val bitbucketUserWrites = new Writes[BitbucketUser] {
    def writes(bitbucketUser: BitbucketUser) = Json.obj(
      "username" -> bitbucketUser.username,
      "id" -> bitbucketUser.id
    )
  }

  def fromJson(userJson: JsValue): BitbucketUser = {
    val id = userJson("account_id").as[String]
    val displayName = userJson("nickname").as[String]
    BitbucketUser(id, displayName)
  }


}
