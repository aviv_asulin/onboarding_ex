package repos

import repos.bitbucket.{BitbucketService, BitbucketSession}
import scala.concurrent.Future

trait Session {
  def getUsers: Future[Iterable[User]]
  def getUserCommits(username: String): Future[Iterable[Commit]]
}

object Session {
  def apply(service: BitbucketService): Session = {
    new BitbucketSession(service)
  }
}