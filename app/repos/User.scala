package repos

import play.api.libs.json.{Json, Writes}

trait User {
  val username: String
  val id: String
}


object User {
  implicit val UserWrites = new Writes[User] {
    def writes(user: User) = Json.obj(
      "username" -> user.username,
      "id" -> user.id
    )
  }
}